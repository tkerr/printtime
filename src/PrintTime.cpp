/******************************************************************************
 * PrintTime.cpp
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * PrintTime class for printing timestamps in the Arduino environment.
 * 
 * Designed for use in real-time environments.  Avoids using formatting
 * functions that require lots of CPU cycles, including integer division.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "PrintTime.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SEVENTY_YEARS 2208988800UL  //!< Time from NTP epoch to Unix epoch


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
PrintTimeClass PrintTime;  //!< Global PrintTime object for the application to use 

 
/******************************************************************************
 * Local data.
 ******************************************************************************/
static const char* DIGITS[] = 
{
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
    "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
    "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
    "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
    "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
    "60", "61", "62", "63", "64", "65", "66", "67", "68", "69",
    "70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
    "80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99",
};

// Pre-formatted timestamp string.  Just fill in the blanks.
static char ts[20] = {0, 0, 0, 0, '-', 0, 0, '-', 0, 0, ' ', 0, 0, ':', 0, 0, ':', 0, 0, '\0'};

static char* str;


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
/**************************************
 * PrintTimeClass::print
 **************************************/
void PrintTimeClass::print(time_t time, time_epoch_t epoch)
{
    tmElements_t myTime;
    if (epoch == EPOCH_NTP)
    {
        time -= SEVENTY_YEARS;
    }
    
    breakTime(time, myTime);
    print(myTime);
}


/**************************************
 * PrintTimeClass::print
 **************************************/
void PrintTimeClass::print(const tmElements_t &tm)
{
    // The tmElements_t Year field is offset from 1970.
    // Assume the year does not go beyond the Unix limit of 2038.
    int year = tm.Year;
    char* str = ts;
    if (year < 30)
    {
        year += 70;
        ts[0] = '1';
        ts[1] = '9';
        ts[2] = DIGITS[year][0];
        ts[3] = DIGITS[year][1];
    }
    else if (year < 130)
    {
        year -= 30;
        ts[0] = '2';
        ts[1] = '0';
        ts[2] = DIGITS[year][0];
        ts[3] = DIGITS[year][1];
    }
    else
    {
        // Year is way beyond Unix limit.
        Serial.print(year + 1970);
        str = &ts[4];
    }
    ts[5]  = DIGITS[tm.Month][0];
    ts[6]  = DIGITS[tm.Month][1];
    ts[8]  = DIGITS[tm.Day][0];
    ts[9]  = DIGITS[tm.Day][1];
    ts[11] = DIGITS[tm.Hour][0];
    ts[12] = DIGITS[tm.Hour][1];
    ts[14] = DIGITS[tm.Minute][0];
    ts[15] = DIGITS[tm.Minute][1];
    ts[17] = DIGITS[tm.Second][0];
    ts[18] = DIGITS[tm.Second][1];
    Serial.print(str);
}


/**************************************
 * PrintTimeClass::println
 **************************************/
void PrintTimeClass::println(time_t time, time_epoch_t epoch)
{
    print(time, epoch);
    Serial.println();
}


/**************************************
 * PrintTimeClass::println
 **************************************/
void PrintTimeClass::println(const tmElements_t &tm)
{
    print(tm);
    Serial.println();
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

 
// End of file.
