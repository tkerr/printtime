/******************************************************************************
 * PrintTime.h
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * PrintTime class for printing timestamps in the Arduino environment.
 * 
 * Designed for use in real-time environments.  Avoids using formatting
 * functions that require lots of CPU cycles.
 */

#ifndef _PRINT_TIME_H
#define _PRINT_TIME_H

// If defined, then support the tmElements_t struct in TimeLib.h
#define USE_TIME_LIB

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>
#include <time.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "TimeLib.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

/**
 * @brief
 * The epoch used by the timestamp being printed
 */
typedef enum
{
    EPOCH_NTP = 0,
    EPOCH_UNIX
} time_epoch_t;


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
class PrintTimeClass
{
public:

    /**
     * @brief
     * Print formatted time from a time_t variable.  Format is YYYY-MM-DD hh:mm:ss
     *
     * @param time The time to print, interpreted as seconds since Jan 1 of the epoch year.
     *
     * @param epoch The timestamp epoch, either NTP or Unix.  Default is the Unix epoch of 1970.
     */
    void print(time_t time, time_epoch_t epoch = EPOCH_UNIX);
    
    /**
     * @brief
     * Print formatted time from a tmElements_t structure.  Format is YYYY-MM-DD hh:mm:ss
     *
     * @param tm The time to print.  Note that the tmElements_t structure defines its Year
     * field to be relative to the Unix epoch of 1970.
     */
    void print(const tmElements_t &tm);
    
    /**
     * @brief
     * Print formatted time from a time_t variable.  Format is YYYY-MM-DD hh:mm:ss.
     * An end-of-line termination sequence is printed after the time.
     *
     * @param time The time to print, interpreted as seconds since Jan 1 of the epoch year.
     *
     * @param epoch The timestamp epoch, either NTP or Unix.  Default is the Unix epoch of 1970.
     */
    void println(time_t time, time_epoch_t epoch = EPOCH_UNIX);
    
    /**
     * @brief
     * Print formatted time from a tmElements_t structure.  Format is YYYY-MM-DD hh:mm:ss.
     * An end-of-line termination sequence is printed after the time.
     *
     * @param tm The time to print.  Note that the tmElements_t structure defines its Year
     * field to be relative to the Unix epoch of 1970.
     */
    void println(const tmElements_t &tm);
    
protected:
private:
};


// Define a single PrintTime object for use by the application.
extern PrintTimeClass PrintTime;

#endif // _PRINT_TIME_H
