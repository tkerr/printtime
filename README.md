# PrintTime #
PrintTime class for printing timestamps in the Arduino environment.

Designed for use in real-time environments.  Avoids using formatting
functions that require lots of CPU cycles.

### Documentation ###
Documentation is contained in the PrintTime.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License (MIT). 
See http://opensource.org/licenses/MIT
